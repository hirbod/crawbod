package storage

import (
	"github.com/go-redis/redis"
	log "github.com/sirupsen/logrus"
	"gitlab.com/hirbod/crawbod/common"
)

var (
	redisClient *redis.Client
)

func SetupRedis() {

	cfg := common.GetConfig()

	removeCacheDep := func() {
		cfg.CrawlerCfg.SkipCache = true
		log.Warnf("cacheCfg is empty. skipping cache")
	}

	if cfg.CacheCfg == nil {
		removeCacheDep()
		return
	}

	redisClient = redis.NewClient(&redis.Options{
		Addr:     cfg.CacheCfg.DSN,
		Password: "",
		DB:       0,
	})

	if _, err := redisClient.Ping().Result(); err != nil {
		log.Warnf("failed to setup redis. err: %v", err)
		removeCacheDep()
	} else {
		log.Infof("redis setup succeeded")
	}
}

func GetRedisClient() *redis.Client {
	return redisClient
}

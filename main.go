package main

import (
	"context"
	"fmt"
	"gitlab.com/hirbod/crawbod/common"
	"gitlab.com/hirbod/crawbod/model"
	pb "gitlab.com/hirbod/crawbod/pb/crawbod"
	"gitlab.com/hirbod/crawbod/storage"
	"log"
	"os"
	"strings"
)

func main() {
	//url := "https://blog.hibsbod.com/"
	//url := "https://www.monzo.com/"
	//url := "https://www.income.com.sg/"

	accessKey := os.Getenv("ACCESS_KEY")
	if accessKey == "" {
		accessKey = "b5ca7522e9c68fc54d67fd79a176271c"
	}

	common.RegisterCfg(accessKey)
	common.SetupLogger()
	storage.SetupRedis()

	Run()
}

func Run() {
	var url string
	fmt.Print("Url: ")
	fmt.Scanln(&url)
	if url == "" {
		log.Panic("invalid url")
	}

	crawler := model.NewCrawlerDM(context.Background())

	// a very ugly cli
	crawled := crawler.CheckHistory(url)
	if crawled != nil {

		fmt.Print("Addr has already been crawled. Force a new crawl? (y/N) ")

		var forceCrawl string
		fmt.Scanln(&forceCrawl)

		if strings.ToLower(forceCrawl) == "y" {
			crawled = crawler.Crawl(url)
		}
	} else {
		crawled = crawler.Crawl(url)
	}

	printCrawledPath(crawled)
}

func printCrawledPath(crawled *pb.CrawledLinks) {
	if crawled == nil {
		return
	}

	fmt.Println("full list:")
	for _, subDomains := range crawled.SubDomainLink {

		fmt.Println(subDomains)
	}
}

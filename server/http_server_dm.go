package server

import (
	"fmt"
	"gitlab.com/hirbod/crawbod/common"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

type HttpServerDM struct {
}

func NewHttpServer() Server {
	return &HttpServerDM{}
}

func (dm *HttpServerDM) Run() error {

	r := gin.Default()
	r.Use(Logger(), gin.Recovery(), cors.Default())

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})

	addr := fmt.Sprintf(":%v", common.GetConfig().ServerCfg.Port)
	if err := r.Run(addr); err != nil {
		return err
	}
	return nil
}

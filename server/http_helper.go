package server

import (
	"gitlab.com/hirbod/crawbod/common"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
)

func Logger() gin.HandlerFunc {
	return gin.LoggerWithWriter(common.GetLogger())
}

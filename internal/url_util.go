package internal

import (
	"golang.org/x/net/html"
	"strings"
)

func SanitizeUrl(url string) string {
	return strings.TrimSuffix(url, "/")
}

func formatUrl(url, link string) string {
	url = SanitizeUrl(url)

	if strings.HasPrefix(link, "/") {
		return url + link
	}
	return link
}

func isSubDomain(url, link string) bool {
	url = strings.TrimSuffix(url, "/")
	return strings.HasPrefix(link, url)
}

func extractLinksFromToken(token html.Token, url string) (string, bool) {
	for _, attr := range token.Attr {
		if attr.Key == "href" {
			link := attr.Val
			link = formatUrl(url, link)
			return link, isSubDomain(url, link)
		}
	}
	return "", false
}

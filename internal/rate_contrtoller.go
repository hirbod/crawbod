package internal

import (
	log "github.com/sirupsen/logrus"
	"time"
)

type RateController struct {
	rc   chan bool
	done chan bool
}

func (controller *RateController) run(du time.Duration) {
	ticker := time.NewTicker(time.Duration(du))
	go func() {
		for {
			select {
			case <-controller.done:
				return
			case <-ticker.C:
				controller.rc <- true
			}
		}
	}()
}

func (controller *RateController) GetToken() bool {
	return <-controller.rc
}

func (controller *RateController) Done() {
	controller.done <- true
}

// SetupRCChan is to create a rate control channel to handle rate number of requests every seconds
// It supports front loading of the requests for 1 second, and continued request in milliseconds
// du Min == 1 | du Max == 1000
func SetupRCChan(rate int64) *RateController {
	if rate < 1 || rate > 1000 {
		log.Warnf("rate is set wrong. rate: %v", rate)
		return nil
	}

	rateController := &RateController{
		rc:   make(chan bool, rate),
		done: make(chan bool),
	}

	du := time.Duration((1000 / rate) * int64(time.Millisecond))
	rateController.run(du)

	return rateController
}

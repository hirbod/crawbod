package internal

import (
	log "github.com/sirupsen/logrus"
	"net/http"
	"time"
)

var (
	httpClient *http.Client
)

func genReuseHttpClient(timeout int32) *http.Client {
	httpClient = &http.Client{Timeout: time.Duration(timeout) * time.Millisecond}
	tr := &http.Transport{
		DisableKeepAlives:   false,
		MaxIdleConnsPerHost: 500,
	}
	httpClient.Transport = tr
	return httpClient
}

func getReuseHttpClient() *http.Client {
	if httpClient == nil {
		httpClient = genReuseHttpClient(5000)
	}
	return httpClient
}

func httpGet(url string) (*http.Response, error) {
	timeStart := time.Now()
	defer func() {
		log.Infof("http get. url: %v, proctm: %v", url, time.Now().Sub(timeStart))
	}()

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	httpHeader := http.Header{}
	httpHeader.Add("User-Agent", "crawler v1.0 - This is a bot") // probably make it configurable!
	httpHeader.Add("Accept-Language", "en-gb")
	req.Header = httpHeader

	client := getReuseHttpClient()

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer func() {
		log.Infof("statusCode: %v", resp.StatusCode)
	}()

	return resp, err
}

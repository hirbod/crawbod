package internal

import (
	"golang.org/x/net/html"
)

func Fetch(baseUrl, url string) (subDomainLinks []string, externalLinks []string, err error) {
	resp, err := httpGet(url)
	if err != nil {
		return nil, nil, err
	}
	defer resp.Body.Close()

	tokenizer := html.NewTokenizer(resp.Body)
	for {
		tokenType := tokenizer.Next()

		if tokenType == html.ErrorToken {
			break
		}

		token := tokenizer.Token()
		switch tokenType {
		case html.StartTagToken:
			if token.DataAtom.String() != "a" {
				continue
			}

			if link, isSubDomain := extractLinksFromToken(token, baseUrl); isSubDomain {
				subDomainLinks = append(subDomainLinks, link)
			} else {
				externalLinks = append(externalLinks, link)
			}
		}
	}

	return subDomainLinks, externalLinks, nil
}

proto:
	protoc --go_out=. pb/*.proto

gen:
	go generate ./model
	go generate ./service

setup:
	go mod tidy
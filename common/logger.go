package common

import (
	"fmt"
	log "github.com/sirupsen/logrus"
	"os"
	"time"
)

func SetupLogger() (file *os.File) {
	config := GetConfig()

	log.SetFormatter(&log.JSONFormatter{})
	log.SetLevel(log.DebugLevel)

	if config.Env != TEST {
		// TODO: probably set a better log rotation!
		logName := fmt.Sprintf("data.log.%v", time.Now().Local().Format("20060102"))
		var err error
		file, err = os.OpenFile(logName, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
		if err != nil {
			log.Panicf("failed to open log file. err: %v", err)
		}

		log.SetOutput(file)
		log.SetLevel(log.InfoLevel)
		log.SetReportCaller(true)
	}

	return file
}

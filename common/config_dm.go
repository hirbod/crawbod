package common

import (
	"encoding/json"
	"fmt"
	pb "gitlab.com/hirbod/crawbod/pb/crawbod"
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
)

type Config struct {
	Env        DeploymentEnv `json:"env"`
	ServerCfg  *ServerCfg    `json:"server"`
	CrawlerCfg *CrawlerCfg   `json:"crawler"`
	CacheCfg   *CacheCfg     `json:"cache"`
}
type ServerCfg struct {
	Port int32 `json:"port"`
}

type CrawlerCfg struct {
	HttpRequestRate int64 `json:"http_request_rate"`
	WorkerCount     int32 `json:"worker_count"`
	SkipCache       bool  `json:"skip_cache"`
}

type CacheCfg struct {
	DSN string `json:"dsn"`
}

var config *Config

type GetConfigResp struct {
	Config   []byte `json:"config"`
	DebugMsg string `json:"debug_msg"`
}

func RegisterCfg(accessKey string) {
	if accessKey == "" {
		log.Panicf("invalid config key. key: %v", accessKey)
	}

	url := fmt.Sprintf("https://haacs.hirbod.me/config/%v", accessKey)
	resp, err := http.Get(url)
	if err != nil {
		log.Panicf("failed to get config. err: %v", err)
	}
	defer resp.Body.Close()

	byteArray, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Panicf("failed to read body. err: %v", err)
	}

	res := &pb.HaacsCfg{}
	err = json.Unmarshal(byteArray, res)
	if err != nil {
		log.Panicf("failed unmarshal. err: %v", err)
	}

	config = &Config{}
	err = json.Unmarshal(res.GetConfig(), config)
	if err != nil {
		log.Panicf("failed unmarshal. err: %v", err)
	}
	log.Infof("cfg loaded:%v", string(res.GetConfig()))
}

func GetConfig() *Config {
	if config == nil {
		log.Panic("config is not registered yet.")
	}
	return config
}

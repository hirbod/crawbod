package common

type DeploymentEnv int32
const (
	TEST DeploymentEnv = iota+1
	PRODUCTION
)

package common

import "errors"

const (
	ErrUnknown             = "unknown error"
	ErrServiceNotFound     = "service not found"
	ErrMarshal             = "error marshal"
	ErrConfigNotRegistered = "config has not been registered"
	ErrTokenizing          = "error tokenizing html"
)

func NewErr(errMsg string) error {
	// TODO: return cerr in future!

	switch errMsg {
	case ErrServiceNotFound:
		return errors.New(ErrServiceNotFound)
	case ErrMarshal:
		return errors.New(ErrMarshal)
	case ErrTokenizing:
		return errors.New(ErrTokenizing)
	default:
		return errors.New(ErrUnknown)

	}
}

package model

import (
	"context"
	"fmt"
	"gitlab.com/hirbod/crawbod/common"
	"gitlab.com/hirbod/crawbod/internal"
	pb "gitlab.com/hirbod/crawbod/pb/crawbod"
	"gitlab.com/hirbod/crawbod/service"
	"gitlab.com/hirbod/crawbod/storage"
	"google.golang.org/protobuf/proto"

	log "github.com/sirupsen/logrus"
	"strings"
	"sync"
	"time"
)

type CrawlerDM struct {
	ctx               context.Context
	crawledLinksRedis service.CrawledLinksRedis

	url            string
	rc             *internal.RateController
	urlChan        chan string
	workerPoolSize int32
	idleWorkerChan chan bool
	wg             sync.WaitGroup
	mx             sync.RWMutex

	visitedSubdomainLinks map[string]bool
	externalLinks         map[string]bool
}

func NewCrawlerDM(ctx context.Context) Crawler {
	return NewCrawlerWithServiceDM(ctx,
		service.NewCrawledLinksRedisDM(ctx, storage.GetRedisClient()),
	)
}

func NewCrawlerWithServiceDM(ctx context.Context, crawledRedis service.CrawledLinksRedis) Crawler {
	return &CrawlerDM{
		ctx:                   ctx,
		crawledLinksRedis:     crawledRedis,
		rc:                    internal.SetupRCChan(common.GetConfig().CrawlerCfg.HttpRequestRate),
		urlChan:               make(chan string, 1000000),
		workerPoolSize:        common.GetConfig().CrawlerCfg.WorkerCount,
		idleWorkerChan:        make(chan bool, common.GetConfig().CrawlerCfg.WorkerCount),
		visitedSubdomainLinks: make(map[string]bool),
		externalLinks:         make(map[string]bool),
	}
}

func (dm *CrawlerDM) CheckHistory(url string) *pb.CrawledLinks {
	if common.GetConfig().CrawlerCfg.SkipCache {
		return nil
	}
	crawled, err := dm.crawledLinksRedis.GetByUrl(url)
	if err != nil {
		log.Error(err)
		return nil
	}
	return crawled
}

func (dm *CrawlerDM) Crawl(url string) *pb.CrawledLinks {
	dm.url = url

	// feed the seed
	dm.urlChan <- dm.url
	dm.visitedSubdomainLinks[dm.url] = true

	// setup workers
	for i := int32(0); i < dm.workerPoolSize; i++ {
		dm.wg.Add(1)
		go func() {
			defer dm.wg.Done()
			dm.crawlWorkers()
		}()
	}
	dm.wg.Wait()

	var subDomainLinks, externalDomainLinks []string

	for k := range dm.visitedSubdomainLinks {
		subDomainLinks = append(subDomainLinks, k)
	}
	for k := range dm.externalLinks {
		externalDomainLinks = append(externalDomainLinks, k)
	}

	crawled := &pb.CrawledLinks{
		Url:                 proto.String(dm.url),
		SubDomainLink:       subDomainLinks,
		ExternalDomainLinks: externalDomainLinks,
	}

	if !common.GetConfig().CrawlerCfg.SkipCache {
		dm.crawledLinksRedis.SetCrawledUrl(crawled)
	}
	return crawled
}

func (dm *CrawlerDM) crawlWorkers() {

	for {
		select {
		case <-time.After(time.Second * 1):
			if len(dm.idleWorkerChan) == 0 { // all workers are  waiting
				return
			}
		case url := <-dm.urlChan:
			dm.idleWorkerChan <- true

			dm.rc.GetToken()
			subDomainLinks, externalLinks, err := internal.Fetch(dm.url, url)
			if err != nil {
				log.Errorf("error fetching url data. url: %v, err: %v", url, err)
				<-dm.idleWorkerChan
				continue
			}

			dm.mx.Lock()
			log.Infof("size of url chan: %v", len(dm.urlChan))
			for _, link := range subDomainLinks {
				if _, ok := dm.visitedSubdomainLinks[link]; !ok {
					dm.visitedSubdomainLinks[link] = true
					dm.urlChan <- link
				}
			}
			for _, link := range externalLinks {
				dm.externalLinks[link] = true
			}
			fmt.Println("crawled: ", url)
			dm.mx.Unlock()
			<-dm.idleWorkerChan
		}
	}
}

func (dm *CrawlerDM) String() string {
	var sb strings.Builder

	for k := range dm.visitedSubdomainLinks {
		sb.WriteString(k)
	}
	return sb.String()
}

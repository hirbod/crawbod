package model

import pb "gitlab.com/hirbod/crawbod/pb/crawbod"

//go:generate mockgen -destination ../mock/model/crawler_dm_mock.go -source ./crawler.go

type Crawler interface {
	Crawl(url string) *pb.CrawledLinks
	CheckHistory(url string) *pb.CrawledLinks
	String() string
}

package service

import (
	pb "gitlab.com/hirbod/crawbod/pb/crawbod"
)

//go:generate mockgen -destination ../mock/service/crawled_links_redis_mock.go -source ./crawled_links_redis.go

type CrawledLinksRedis interface {
	GetByUrl(url string) (*pb.CrawledLinks, error)
	SetCrawledUrl(crawled *pb.CrawledLinks) error
}

package service

import (
	"context"
	"github.com/go-redis/redis"
	pb "gitlab.com/hirbod/crawbod/pb/crawbod"
	"google.golang.org/protobuf/proto"
)

type CrawledLinksRedisDM struct {
	ctx    context.Context
	client *redis.Client
}

func NewCrawledLinksRedisDM(ctx context.Context, client *redis.Client) CrawledLinksRedis {
	return &CrawledLinksRedisDM{
		ctx:    ctx,
		client: client,
	}
}

func (dm *CrawledLinksRedisDM) GetByUrl(url string) (*pb.CrawledLinks, error) {
	val, err := dm.client.Get(url).Result()
	if err != nil {
		return nil, err
	}

	crawled := new(pb.CrawledLinks)
	err = proto.Unmarshal([]byte(val), crawled)
	if err != nil {
		return nil, err
	}
	return crawled, nil
}

func (dm *CrawledLinksRedisDM) SetCrawledUrl(crawled *pb.CrawledLinks) error {
	buf, err := proto.Marshal(crawled)
	if err != nil {
		return err
	}

	dm.client.Set(crawled.GetUrl(), buf, 0)

	return nil
}

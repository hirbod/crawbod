module gitlab.com/hirbod/crawbod

go 1.16

require (
	github.com/gin-contrib/cors v1.4.0
	github.com/gin-gonic/gin v1.8.1
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/golang/mock v1.6.0
	github.com/onsi/ginkgo v1.16.5 // indirect
	github.com/onsi/gomega v1.19.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.8.0
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f
	google.golang.org/protobuf v1.28.0
)

package test

import (
	"gitlab.com/hirbod/crawbod/common"
	"os"
	"testing"
)

func TestMain(m *testing.M) {

	accessKey := os.Getenv("ACCESS_KEY")
	if accessKey == "" {
		accessKey = "b5ca7522e9c68fc54d67fd79a176271c"
	}
	common.RegisterCfg(accessKey)

	code := m.Run()
	os.Exit(code)
}

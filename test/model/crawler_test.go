package test

import (
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/hirbod/crawbod/common"
	mock_service "gitlab.com/hirbod/crawbod/mock/service"
	"gitlab.com/hirbod/crawbod/model"
	pb "gitlab.com/hirbod/crawbod/pb/crawbod"
	"sort"
	"testing"
)

var (
	ValidUrl        = "https://blog.hibsbod.com"
	InvalidUrl      = "https://wrong.hibsbod.com"
	ActualUrl       = "https://www.hibsbod.com"
	ValidResultList = []string{
		"https://blog.hibsbod.com",
		"https://blog.hibsbod.com/123",
	}
	InvalidResultList = []string{
		"https://wrong.hibsbod.com",
	}
	ActualBlogResList = []string{
		"https://www.hibsbod.com",
	}
	ActualExternalList = []string{
		"https://www.facebook.com/HirbodLoTuS/",
		"https://blog.hibsbod.com/",
		"https://gitlab.com/hirbod",
		"https://www.linkedin.com/in/hirbod/",
		"mailto:hirbod@hotmail.my",
	}
)

/*
	TODO: mock http get!
*/

func TestCrawlDMCheckHistory(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockRedis := mock_service.NewMockCrawledLinksRedis(mockCtrl)
	crawlerDM := model.NewCrawlerWithServiceDM(context.Background(), mockRedis)

	testCases := []struct {
		testCaseName  string
		url           string
		err           error
		subDomainList []string
		prep          func()
	}{
		{
			testCaseName:  "normal valid testcase",
			url:           ValidUrl,
			err:           nil,
			subDomainList: ValidResultList,
			prep: func() {
				crawled := &pb.CrawledLinks{
					Url:                 &ValidUrl,
					SubDomainLink:       ValidResultList,
					ExternalDomainLinks: nil,
				}
				mockRedis.EXPECT().GetByUrl(ValidUrl).Return(crawled, nil)
			},
		},
		{
			testCaseName:  "normal empty testcase",
			url:           InvalidUrl,
			err:           nil,
			subDomainList: InvalidResultList,
			prep: func() {
				crawled := &pb.CrawledLinks{
					Url:                 &InvalidUrl,
					SubDomainLink:       InvalidResultList,
					ExternalDomainLinks: nil,
				}
				mockRedis.EXPECT().GetByUrl(InvalidUrl).Return(crawled, nil)
			},
		},
	}

	for _, c := range testCases {
		t.Run(c.testCaseName, func(t *testing.T) {
			c.prep()
			crawled := crawlerDM.CheckHistory(c.url)
			assert.Equal(t, c.subDomainList, crawled.SubDomainLink, "failed to GetByUrl")
		})
	}
}

func TestCrawlDMCrawl(t *testing.T) {
	mockCtrl := gomock.NewController(t)
	defer mockCtrl.Finish()

	mockRedis := mock_service.NewMockCrawledLinksRedis(mockCtrl)
	crawlerDM := model.NewCrawlerWithServiceDM(context.Background(), mockRedis)

	common.GetConfig().CrawlerCfg.SkipCache = true

	testCases := []struct {
		testCaseName  string
		url           string
		err           error
		subDomainList []string
		externalLinks []string
		prep          func()
	}{
		{
			testCaseName:  "normal valid testcase",
			url:           ActualUrl,
			err:           nil,
			subDomainList: ActualBlogResList,
			externalLinks: ActualExternalList,
			prep: func() {

			},
		},
	}

	for _, c := range testCases {
		t.Run(c.testCaseName, func(t *testing.T) {
			c.prep()
			crawled := crawlerDM.Crawl(c.url)

			assert.Equal(t, sortSlice(c.subDomainList), sortSlice(crawled.SubDomainLink), "failed subdomains")
			assert.EqualValues(t, sortSlice(c.externalLinks), sortSlice(crawled.ExternalDomainLinks), "failed external domains")
		})
	}
}

/*
GetByUrl(url string) (*pb.CrawledLinks, error)
	SetCrawledUrl(crawled *pb.CrawledLinks) error
*/

func sortSlice(l []string) []string {
	sort.Slice(l, func(i, j int) bool {
		return l[i] < l[j]
	})
	return l
}
